#!/usr/bin/env python

"""
Author: Bryan Quach
"""

from toolbox import Database, Calculator
from os import makedirs
from os.path import exists, splitext, basename
import numpy as np
import pysam
import re


def output_sample_info(outfile, data):
    """

    """
    fout = open(outfile, 'w')
    tmp = data[data.keys()[0]]
    full_labels = tmp[3].keys()
    joined_labels = tmp[4].keys()
    header = "SAMPLE\tTOTAL_PASSED_QC\tTOTAL_MISMATCH\t" + "\t".join(full_labels) + "\t"
    header += "\t".join(["AGGREGATED_"+x for x in joined_labels]) + "\t"
    header += "\t".join(["RATE_"+x for x in joined_labels]) + "\n"
    fout.write(header)
    for sample in data.keys():
        outline = [sample]
        total_qc = str(data[sample][1])
        total_mm = str(sum(data[sample][2]))
        outline.append(total_qc)
        outline.append(total_mm)
        for label in full_labels:
            mm_count = str(sum(data[sample][3][label]))
            outline.append(mm_count)
        for label in joined_labels:
            mm_count = str(sum(join_ambiguous_types(data[sample][3])[label]))
            outline.append(mm_count)
        for label in joined_labels:
            mm_count = str(data[sample][4][label])
            outline.append(mm_count)
        fout.write("\t".join(outline) + "\n")
    fout.close()
    return


def main(args):

    calculator = Calculator(Database(args.aln_file, args.read_length))
    calculator.compute_mm()
    calculator.compute_double_mad()
    db = calculator.get_db()
    db.reload_mm_dict()
    db.pickle_data()
    #outfile = args.out_dir + "/" + basename(splitext(args.samples_file)[0]) + ".out"
    #output_sample_info(outfile, sample_info)
    return

if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser(prog='Mismatch Calculator',
        usage='python calculate_mismatches.py [options] aln_file read_length',
        description='Tabulate per base read mismatch information.')
    parser.add_argument('aln_file',
        type=str,
        help='SAM/BAM file with MD and NM tags that follow the format used by '
            'SAMTools calmd.')
    parser.add_argument('read_length',
        type=int,
        help="Length of the reads in the sample.")
    parser.add_argument('-o', '--out-dir',
        default='.',
        type=str,
        help='Path for output. DEFAULT: working directory')
    args = parser.parse_args()

    if not exists(args.out_dir):
        makedirs(args.out_dir)
    main(args)

