#!/usr/bin/env python

"""
Author: Bryan Quach

Filters reads based on several criteria and write the reads that pass QC into
a new BAM file.
"""

from os.path import splitext
import pysam
import re


def load_aln(filename):
    """Load BAM/SAM file.

    Args:
        filename: A string specifying the path and name of the BAM or SAM file 
            to load as an AlignmentFile object. 

    Returns: An AlignmentFile object.
    """
    file_ext = splitext(filename)[1].lower()
    if file_ext == ".sam":
        aln = pysam.AlignmentFile(filename, mode='r') 
    elif file_ext == ".bam":
        aln = pysam.AlignmentFile(filename, mode='rb') 
    else:
        raise ValueError, "Input file must have '.bam' or '.sam' extension"
    return aln


def open_out_stream(filename, template):
    """Open BAM/SAM file handle.

    Creates a file handle that can be used to write reads to a file.

    Args:
        filename: A string specifying the name of the file to create.
        template: A pysam AlignemtFile object from which the header will be 
            copied for the new file.

    Returns: A file handle.
    """
    file_ext = splitext(filename)[1].lower()
    if file_ext == ".sam":
        aln = pysam.AlignmentFile(filename, mode='w', template=template) 
    elif file_ext == ".bam":
        aln = pysam.AlignmentFile(filename, mode='wb', template=template)
    else:
        raise ValueError, "Output file must have '.bam' or '.sam' extension"
    return aln


def load_whitelist(filename):
    """Get references in the whitelist.

    Loads a list of reference names that acceptable reads come from.

    Args:
        filename: A string specifying the path and name of a file that contains
            the names of acceptable references. Each line should contain only 
            one reference name.

    Returns: A list of acceptable reference names.
    """
    whitelist = []
    fin = open(filename, 'r')
    for line in fin:
        whitelist.append(line.strip().lower())
    fin.close()
    return whitelist


def meets_phred_qc(read, min_phred, min_fraction):
    """Check if alignment passes PHRED quality score filter.

    Args:
        min_phred: An int specifying the lowest PHRED score allowed before a
            base is considered low quality.
        min_fraction: A float specifying the minimum proportion of acceptable
            quality bases required for a read to be retained.

    Returns: A boolean specifying if a read should be retained.
    """
    scores = read.query_alignment_qualities
    hit_rate = len([1 for x in scores if x >= min_phred])/float(len(scores))
    if hit_rate >= min_fraction:
        return True
    return False


def main(args):
    """Main method."""
    in_bam = load_aln(args.input_file)
    out_bam = open_out_stream(args.output_file, in_bam)
    whitelist = load_whitelist(args.whitelist)
    cigar_regex = re.compile('I|H|S|D') #indel and clipping identifiers
    for read in in_bam.fetch(until_eof=True):
        #Check read against all filtering criteria
        if read.is_unmapped: continue
        if read.is_secondary: continue
        if read.is_duplicate: continue
        if read.is_qcfail: continue
        if read.mapping_quality < args.mapq : continue
        if cigar_regex.search(read.cigarstring) is not None: continue
        if not meets_phred_qc(read, args.phred, args.percent/100.0): continue
        if "N" in read.get_tag("MD").upper(): continue #reference contains 'N'
        if "N" in read.query_sequence.upper(): continue #read contains 'N'
        if read.reference_name not in whitelist: continue
        out_bam.write(read)
    in_bam.close()
    out_bam.close()
    return


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser(prog='Alignment Filter',
        usage=('python bam_filter.py [options] input_file output_file '
            'whitelist\n\nUse \'python bam_filter.py -h\' '
            'for more info\n'),
        description='Filter alignments based on several criteria.')
    parser.add_argument('input_file',
        type=str,
        help='SAM/BAM file to filter.')
    parser.add_argument('output_file',
        type=str,
        help='SAM/BAM file for post-filtering output')
    parser.add_argument('--phred',
        default=20,
        type=int,
        help='Minimum PHRED score value used in filtering. DEFAULT: 20')
    parser.add_argument('--percent',
        default=90,
        type=int,
        help=('Percentage of bases in a read that must meet or exceed '
            '--phred for the read to be retained. DEFAULT: 90'))
    parser.add_argument('--mapq',
        default=30,
        type=int,
        help='Minimum MAPQ score allowed. DEFAULT: 30')
    parser.add_argument('whitelist',
        default=None,
        type=str,
        help=('A list of reference names (chr1, chr2, etc.) for alignments to '
            'keep. Each reference should be on its own line.'))
    args = parser.parse_args()

    main(args)

