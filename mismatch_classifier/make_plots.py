#!/usr/bin/env python

"""
Author: Bryan Quach
"""

from toolbox import Database, Calculator, Plotter
from os import makedirs
from os.path import exists, splitext, basename
import numpy as np
import pysam
import re
import matplotlib
matplotlib.use("PDF")
import matplotlib.pyplot as plt
try:
    import cPickle as pickle
except:
    import pickle


def main(args):
    l_mads = []
    r_mads = []
    calculator = Calculator()
    plotter = Plotter()
    fin = open(args.sample_list, 'r')
    dbs = []
    for line in fin:
        pin = open(line.strip(), 'rb')
        dbs.append(pickle.load(pin))
        pin.close()
    for db in dbs:
        calculator.set_db(db)
        plotter.set_db(db)
        m = db.get_median()
        fig1 = plotter.individual_mm_profiles()
        fig2 = plotter.total_mm_profile()
        #plotter.add_line(fig2, np.log10(m-3623.5))
        #plotter.add_line(fig2, np.log10(m+9027))
        plotter.add_line(fig2, np.log10(m-153))
        plotter.add_line(fig2, np.log10(m+217))
        fig1_name = basename(splitext(db.get_filename())[0]) + "_individual_mm.pdf"
        fig2_name = basename(splitext(db.get_filename())[0]) + "_total_mm.pdf"
        fig1.savefig(fig1_name)
        fig2.savefig(fig2_name)
        plt.close(fig1)
        plt.close(fig2)
        #plotter.grouped_mm_profiles()

    print "a = c(" + ",".join(l_mads) + ")"
    print "b = c(" + ",".join(r_mads) + ")"
    #outfile = args.out_dir + "/" + basename(splitext(args.samples_file)[0]) + ".out"
    #output_sample_info(outfile, sample_info)
    return

if __name__ == "__main__":
    from argparse import ArgumentParser
    #args = None
    
    parser = ArgumentParser(prog='Mismatch Profile Plotter',
        usage='python make_plots.py [options] sample_list',
        description='Plot per base read mismatch information.')
    parser.add_argument('sample_list',
        type=str,
        help='A text file containing a list of pickle files (one file per '
            'line).')
    """
    parser.add_argument('read_length',
        type=int,
        help="Length of the reads in the sample.")
    parser.add_argument('-o', '--out-dir',
        default='.',
        type=str,
        help='Path for output. DEFAULT: working directory')
    args = parser.parse_args()

    if not exists(args.out_dir):
        makedirs(args.out_dir)
    """
    args = parser.parse_args()
    main(args)

