#!/usr/bin/env python

"""
Author: Bryan Quach
"""

from scipy.stats import ttest_ind
from statsmodels.sandbox.stats.multicomp import multipletests
from toolbox import Database, Calculator, Plotter
from os import makedirs
from os.path import exists, splitext, basename
import numpy as np
import pysam
import re
try:
    import cPickle as pickle
except:
    import pickle


def main(args):
    calculator = Calculator()
    plotter = Plotter()
    fin = open(args.sample_list, 'r')
    db_group1 = []
    db_group2 = []
    for line in fin:
        line_data = line.strip().split("\t")
        pin = open(line_data[0], 'rb')
        calculator.set_db(pickle.load(pin))
        calculator.find_outliers(lo=args.lo, hi=args.hi)
        if line_data[1] == "1":
            db_group1.append(calculator.get_grouped_mm_rates(True))
        else:
            db_group2.append(calculator.get_grouped_mm_rates(True))
        pin.close()
    #print db_group1
    #print db_group2
    mm_types = db_group1[0].keys()
    pvals = []
    for mm_type in mm_types:
        group1 = [db_group1[i][mm_type] for i in range(len(db_group1))]
        group2 = [db_group2[i][mm_type] for i in range(len(db_group2))]
        results = ttest_ind(group1, group2, equal_var=False)
        pvals.append(results[1]/2)
    qvals = multipletests(pvals, alpha=0.05, method='fdr_by')[1]
    print "Labels:\t" + "\t".join(mm_types)
    print "p-values:\t" + "\t".join([str(x) for x in pvals])
    print "q-values:\t" + "\t".join([str(x) for x in qvals])

    #outfile = args.out_dir + "/" + basename(splitext(args.samples_file)[0]) + ".out"
    #output_sample_info(outfile, sample_info)
    return

if __name__ == "__main__":
    from argparse import ArgumentParser
    #args = None
    
    parser = ArgumentParser(prog='Group Comparer',
        usage='python compare_groups.py [options] sample_list',
        description='Compare rates of mismatches between specified groups.')
    parser.add_argument('sample_list',
        type=str,
        help='A text file containing a column of pickle files and their '
            'corresponding group label in a second column')
    parser.add_argument('-lo',
        type=float,
        help=".")
    parser.add_argument('-hi',
        type=float,
        help='')
    args = parser.parse_args()
    """
    if not exists(args.out_dir):
        makedirs(args.out_dir)
    """
    args = parser.parse_args()
    main(args)

