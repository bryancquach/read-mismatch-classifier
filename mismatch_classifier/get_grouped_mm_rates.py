#!/usr/bin/env python

"""
Author: Bryan Quach
"""

from toolbox import Database, Calculator
from os import makedirs
from os.path import exists, splitext, basename
import numpy as np
import pysam
import re


def get_thresholds(filename):
    filter_dict = {}
    fin = open(filename, 'r')
    fin.readline() #skip header
    for line in fin:
        line_data = line.strip().split('\t')
        name_bits = line_data[0].split("_")
        sample_name = name_bits[0].upper() + "_" + name_bits[1]
        lower_bound = float(line_data[2])
        upper_bound = float(line_data[3])
        filter_dict[sample_name] = (lower_bound, upper_bound)
    fin.close()
    return filter_dict


def main(args):
    calculator = Calculator(Database(args.aln_file, args.read_length))
    calculator.compute_mm()
    if(args.threshold_file is None):
        mm_rates = calculator.get_grouped_mm_rates()
    else:
        cutoffs_dict = get_thresholds(args.threshold_file)
        name_bits = basename(splitext(args.aln_file)[0]).split("_")
        sample_name = name_bits[3].split(".")[0].upper() + "_" + name_bits[2]
        cutoffs = cutoffs_dict[sample_name]
        calculator.set_outliers(lo=cutoffs[0], hi=cutoffs[1])
        mm_rates = calculator.get_grouped_mm_rates(use_filter=True)
    mm_types = mm_rates.keys()
    mm_types.sort()
    filename = args.out_dir + "/" + basename(splitext(args.aln_file)[0]).split(".")[0] + ".grouped_mm_rates.txt"
    fout = open(filename, 'w')
    for mm in mm_types:
        fout.write("\t".join([mm, str(mm_rates[mm])]) + "\n")
    fout.close()
    return

if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser(prog='Grouped Mismatch Rate Calculator',
        usage='python get_grouped_mm_rates.py [options] aln_file read_length',
        description='Tabulate per sample mismatch rates.')
    parser.add_argument('aln_file',
        type=str,
        help='SAM/BAM file with MD and NM tags that follow the format used by '
            'SAMTools calmd.')
    parser.add_argument('read_length',
        type=int,
        help="Length of the reads in the sample.")
    parser.add_argument('-o', '--out-dir',
        default='.',
        type=str,
        help='Path for output. DEFAULT: working directory')
    parser.add_argument('-t', '--threshold_file',
        default=None,
        type=str,
        help='File containing sample thresholds for outlier detection. Assumes '
            'a header line and tab-delimited format. DEFAULT: None')
    args = parser.parse_args()

    if not exists(args.out_dir):
        makedirs(args.out_dir)
    main(args)

