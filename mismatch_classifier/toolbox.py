#!/usr/bin/env python

"""
Author: Bryan Quach
"""

from copy import deepcopy
from itertools import permutations
from os import makedirs
from os.path import exists, splitext, basename
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pysam
import re
try:
    import cPickle as pickle
except:
    import pickle

class Database(object):
    """Holds mismatch information for a BAM file"""

    __filename = ""
    __aln = None #pysam AlignmentFile object
    __mm_dict = {} #tracks per base mismatches for each type
    __nt_dict = {} #track per base nucleotide counts
    __keepers = [] #non outlier alignment positions
    __aln_count = 0 #total alignment count
    __read_size = 0 #maximum alignment length
    __left_mad = 0
    __right_mad = 0
    __median = 0

    def __init__(self, filename, read_size):
        """Contstructor.

        """
        self.__filename = filename
        self.__aln = self.load_aln(filename)
        print "Tallying total number of alignments..."
        self.__aln_count = self.__aln.count(until_eof=True)
        print "Done!"
        self.reload_aln()
        self.__read_size = read_size
        self.__keepers = range(read_size)
        for mm_type in permutations("ACGT", 2):
            self.__mm_dict["".join(mm_type)] = np.array([0] * read_size)
        for nt in ["A","C","G","T"]:
            self.__nt_dict[nt] = np.array([0] * read_size)


    def __del__(self):
        """Class deconstructor."""
        try:
            self.__aln.close()
        except:
            pass
        return

    @classmethod
    def load_aln(cls, filename):
        """Load BAM/SAM file.
    
        """
        file_ext = splitext(filename)[1].lower()
        if file_ext == ".sam":
            aln = pysam.AlignmentFile(filename, mode='r')
        elif file_ext == ".bam":
            aln = pysam.AlignmentFile(filename, mode='rb')
        else:
            raise ValueError, "Input file must have '.bam' or '.sam' extension"
        return aln

    def reload_mm_dict(self):
        self.__mm_dict = deepcopy(self.__mm_dict)
        return

    def reload_aln(self):
        """Reload BAM/SAM file.

        """
        self.__aln.close()
        self.__aln = self.load_aln(self.__filename)
        return


    def close_aln(self):
        """Close AlignmentFile object."""
        self.__aln.close()
        self.__aln = None


    def get_aln_count(self):
        return self.__aln_count


    def get_filename(self):
        return self.__filename


    def get_aln_iter(self):
        """Retrieve AlignmentFile iterator."""
        return self.__aln.fetch(until_eof=True)


    def get_read_size(self):
        """Retrieve read size."""
        return self.__read_size


    def get_num_alignments(self):
        """Retrieve total alignment count."""
        return self.__aln_count


    def add_mm_val(self, mm_type, index, value=1):
        self.__mm_dict[mm_type.upper()][index] += value
        return


    def add_nt_val(self, nt, index, value=1):
        self.__nt_dict[nt.upper()][index] += value
        return


    def get_mm_vector(self, mm_type, use_filter=False):
        if use_filter:
            keep = self.__keepers
        else:
            keep = range(self.__read_size)
        return deepcopy(self.__mm_dict[mm_type][keep])


    def get_mm_dict(self, use_filter=False):
        if use_filter:
            keep = self.__keepers
        else:
            keep = range(self.__read_size)
        new_dict = deepcopy(self.__mm_dict)
        for key in new_dict.keys():
            new_dict[key] = new_dict[key][keep]
        return new_dict


    def get_nt_dict(self, use_filter=False):
        if use_filter:
            keep = self.__keepers
        else:
            keep = range(self.__read_size)
        new_dict = deepcopy(self.__nt_dict)
        for key in new_dict.keys():
            new_dict[key] = new_dict[key][keep]
        return new_dict


    def get_left_mad(self):
        return self.__left_mad


    def get_right_mad(self):
        return self.__right_mad


    def get_median(self):
        return self.__median


    def set_left_mad(self, value):
        self.__left_mad = value
        return


    def set_right_mad(self, value):
        self.__right_mad = value
        return


    def set_median(self, value):
        self.__median = value
        return


    def set_keepers(self, keep_list):
        self.__keepers = keep_list
        return


    def get_keepers(self):
        return self.__keepers


    def pickle_data(self, filename=None):
        """Save object as pickle file."""
        self.__aln.close()
        self.__aln = None
        if filename is None:
            filename = splitext(self.__filename)[0] + ".pkl"
        fout = open(filename, 'wb')
        pickle.dump(self, fout)
        fout.close()
        return


class Calculator(object):
    """Performs mismatch calculations."""
    
    __mm_db = None #Database object


    def __init__(self, db=None):
        """Constructor."""
        if db is not None:
            self.__mm_db = db


    def get_db(self):
        return self.__mm_db
    

    def set_db(self, db):
        self.__mm_db = db
        return


    @classmethod
    def complement(cls, sequence):
        """Get the complement of the given DNA sequence.
           
        Args:
            sequence: A string from alphabet {A,C,G,T,N,a,c,g,t,n}.
    
        Returns:
            A string with the complementary DNA sequence of the sequence 
            provided.
    
        Raises:
            KeyError: The string provided contains characters other than 
                'A', 'C', 'G', 'T', 'N', 'a', 'c', 'g', 't', and 'n'.
        """
        seq_dict = {'A':'T', 'T':'A', 'G':'C', 'C':'G', 'N':'N',
            'a':'T', 't':'A', 'g':'C', 'c':'G', 'n':'N',}
        try:
            return "".join([seq_dict[i] for i in sequence])
        except KeyError:
            error_msg = "DNA sequence must be from {A,C,G,T,N,a,c,g,t,n}"
            raise KeyError(error_msg)


    def compute_mm(self):
        aln_iter = self.__mm_db.get_aln_iter()
        seq_length = self.__mm_db.get_read_size()
        md_regex = re.compile(r'(\d+)([A-z]+)')
        for read in aln_iter:
            current_index = 0
            if read.get_tag("NM") == 0: continue #skip reads with no mismatch
            tag_value = read.get_tag("MD")
            code = md_regex.findall(tag_value)
            seq = read.query_sequence        
            for digit, ref_base in code: 
                current_index += int(digit)
                if read.is_reverse:
                    key = self.complement(ref_base + seq[current_index])
                    update_index = seq_length-current_index-1
                    self.__mm_db.add_mm_val(key, update_index)
                    self.__mm_db.add_nt_val(ref_base, update_index)
                else:
                    key = ref_base.upper() + seq[current_index]
                    self.__mm_db.add_mm_val(key, current_index)
                    self.__mm_db.add_nt_val(ref_base, current_index)
                current_index += 1
        return


    def compute_double_mad(self):
        """Calculate left and right MAD.
    
        """
        mm_list = self.get_total_mm_vector()
        m = np.median(mm_list)
        abs_dev = np.abs(mm_list-m)
        left_mad = np.median(abs_dev[np.where(mm_list <= m)])
        right_mad = np.median(abs_dev[np.where(mm_list >= m)])
        if left_mad == 0.0 or right_mad == 0.0:
            raise ValueError, "MAD statistic is 0"
        self.__mm_db.set_left_mad(left_mad)
        self.__mm_db.set_right_mad(right_mad)
        self.__mm_db.set_median(m)
        return
    
    def set_outliers(self, lo, hi):
        mm_list = self.get_total_mm_vector()
        keepers = np.where((mm_list >= lo) & (mm_list <= hi))
        self.__mm_db.set_keepers(keepers)
        return

    def find_outliers(self, lo, hi):
        mm_list = self.get_total_mm_vector()
        m = np.median(mm_list)
        keepers = np.where((mm_list >= m-lo) & (mm_list <= m+hi))
        self.__mm_db.set_keepers(keepers)
        return


    def get_grouped_mm_dict(self, use_filter=False):
        groups = (("AT","TA"),
                  ("CG","GC"),
                  ("GT","CA"),
                  ("TG","AC"),
                  ("CT","GA"),
                  ("TC","AG"))
        mm_dict = self.__mm_db.get_mm_dict(use_filter=use_filter)
        grouped_mm = {}
        for m1,m2 in groups:
            key = m1 + "|" + m2
            grouped_mm[key] = mm_dict[m1] + mm_dict[m2]
        return grouped_mm


    def get_total_mm_vector(self, use_filter=False):
        mm_dict = self.__mm_db.get_mm_dict(use_filter=use_filter)
        if use_filter:
            total = np.array([0]*len(self.__mm_db.get_keepers()))
        else:
            total = np.array([0]*self.__mm_db.get_read_size())
        for k,v in mm_dict.items():
            total += v
        return total


    def get_mm_rates(self, mm_type=None, use_filter=False):
        if mm_type is None:
            mm_dict = self.__mm_db.get_mm_dict(use_filter=use_filter)
            nt_dict = self.__mm_db.get_nt_dict(use_filter=use_filter)
            rate_dict = {}
            for k,v in mm_dict.items():
                #rate_dict[k] = (float(sum(v))/len(v)/float(self.__mm_db.get_aln_count()))*1000000
                rate_dict[k] = float(sum(v))/sum(nt_dict[k[0]])*100
            return rate_dict
        else:
            mm_vector = self.__mm_db.get_mm_vector(mm_type, 
                use_filter=use_filter)
            return float(sum(mm_vector))/sum(nt_dict[mm_type[0]])*100


    def get_grouped_mm_rates(self, use_filter=False):
        grouped_totals = self.get_grouped_mm_dict(use_filter)
        nt_dict = self.__mm_db.get_nt_dict(use_filter=use_filter)
        grouped_mm_rates = {}
        for k,v in grouped_totals.items():
            mm_types = k.split("|")
            nt_total = sum(nt_dict[mm_types[0][0]]) + sum(nt_dict[mm_types[1][0]])
            grouped_mm_rates[k] = (float(sum(v))/nt_total)*100
        return grouped_mm_rates


class Plotter(object):

    __mm_db = None #Database object


    def __init__(self):
        """Constructor."""
        pass


    def get_db(self):
        return deepcopy(self.__mm_db)
    

    def set_db(self, db):
        self.__mm_db = db
        return


    @classmethod
    def add_line(cls, figure, value):
        assert len(figure.get_axes()) > 0, "Empty figure!"
        ax0 = figure.get_axes()[0]
        ax0.axhline(value, color='r', linestyle=':')
        return


    def total_mm_profile(self):
        """Plot total mismatch profile."""
        calc = Calculator(self.__mm_db)
        mm_vec = np.log10(calc.get_total_mm_vector() + 1)
        xvals = range(1,mm_vec.size+1)
        fig = plt.figure(figsize=(12,9))
        ax0 = fig.add_subplot(1,1,1)
        ax0.plot(xvals, mm_vec, 'ok-', label="Total Mismatch Profile", linewidth=3)
        ax0.set_title("Total Mismatches Per Base")
        ax0.set_ylabel("log10(Mismatch Count+1)")
        ax0.set_xlabel("Read Position (5' to 3')")
        return fig


    def individual_mm_profiles(self, types=[]):
        """Plot profile for each mismatch type.
    
        Args:
            types: A list of strings specifying the mismatch types to plot. If
                not specified then all of them will be included.
        """
        mm_dict = self.__mm_db.get_mm_dict()
        if len(types) == 0:
            types = mm_dict.keys()
        fig = plt.figure(figsize=(13,9))
        ax0 = fig.add_subplot(1,1,1)
        ax0.set_title("Mismatch Profiles")
        ax0.set_ylabel("log10(Mismatch Count+1)")
        ax0.set_xlabel("Read Position (5' to 3')")
        colors = plt.cm.get_cmap("jet", len(types))
        i=0
        for key in types:
            mm_vec = np.log10(mm_dict[key] + 1)
            xvals = range(1,mm_vec.size+1)
            ax0.plot(xvals, mm_vec, 'o-', c=colors(i), label=key, linewidth=3)
            i+=1
        box = ax0.get_position()
        ax0.set_position([box.x0, box.y0, box.width * 0.9, box.height])
        ax0.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        return fig


    def grouped_mm_profiles(self, types=[]):
        """Plot profile for grouped mismatch types.
    
        Args:
            types: A list of strings specifying the mismatch groups to plot. If
                not specified then all of them will be included. The group 
                names follow the convention "<type>|<type>". Example: AT|TA
        """
        c = Calculator()
        c.set_db(self.__mm_db)
        mm_dict = c.get_grouped_mm_dict()
        if len(types) == 0:
            types = mm_dict.keys()
        fig = plt.figure(figsize=(13,9))
        ax0 = fig.add_subplot(1,1,1)
        ax0.set_title("Grouped Mismatch Profiles")
        ax0.set_ylabel("log10(Mismatch Count+1)")
        ax0.set_xlabel("Read Position (5' to 3')")
        colors = plt.cm.get_cmap("jet", len(types))
        i=0
        for key in types:
            mm_vec = np.log10(mm_dict[key] + 1)
            xvals = range(1,mm_vec.size+1)
            ax0.plot(xvals, mm_vec, 'o-', c=colors(i), label=key, linewidth=3)
            i+=1
        box = ax0.get_position()
        ax0.set_position([box.x0, box.y0, box.width * 0.9, box.height])
        ax0.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        return fig

