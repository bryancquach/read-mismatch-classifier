#!/usr/bin/env python

"""
Author: Bryan Quach
"""

from itertools import permutations
from os import makedirs
from os.path import exists, splitext, basename
from scipy.stats import ttest_ind
from statsmodels.sandbox.stats.multicomp import multipletests
import matplotlib
matplotlib.use("PDF")
import matplotlib.pyplot as plt
import numpy as np
import pysam
import re
import sys


def load_sample_info(filename):
    """Create dictionary for sample data.

    """
    sample_data = {}
    try:
        fin = open(filename, 'r')
    except IOError:
        raise IOError, "%s cannot be opened" % filename
    for line in fin:
        line_data = line.strip().split('\t')
        fname = line_data[0]
        file_ext = splitext(fname)[1].lower() 
        assert exists(fname), '%s is not a file' % fname
        e_msg = "All files must have a '.bam' or '.sam' extension"
        assert file_ext in [".sam",".bam"], e_msg
        if file_ext == "bam":
            assert exists(fname + ".bai"), "Index not found for %s" % fname
        sample_data[line_data[0]] = [line_data[1]]
    fin.close()
    return sample_data


def load_aln(filename):
    file_ext = splitext(filename)[1].lower()
    if file_ext == "sam":
        aln = pysam.AlignmentFile(filename, mode='r') 
    else:
        aln = pysam.AlignmentFile(filename, mode='rb') 
    return aln


def meets_qc(read, quality, bases):
    """

    """
    scores = read.query_alignment_qualities
    hit_rate = len([1 for x in scores if x >= quality])/float(len(scores))
    if hit_rate >= bases:
        return True
    return False


def peek_length(filename):
    """

    """
    aln = load_aln(filename)
    read = aln.fetch(until_eof=True).next()
    qlen = read.query_alignment_length
    aln.close()
    return qlen

def complement(sequence):
    """Get the complement of the given DNA sequence.
       
    Args:
        sequence: A string from alphabet {A,C,G,T,N,a,c,g,t,n}.

    Returns:
        A string with the complementary DNA sequence of the sequence 
        provided.

    Raises:
        KeyError: The string provided contains characters other than 
            'A', 'C', 'G', 'T', 'N', 'a', 'c', 'g', 't', and 'n'.
    """
    seq_dict = {'A':'T', 'T':'A', 'G':'C', 'C':'G', 'N':'N',
        'a':'T', 't':'A', 'g':'C', 'c':'G', 'n':'N',}
    try:        
        return "".join([seq_dict[i] for i in sequence])
    except KeyError:
        error_msg = "DNA sequence must be from {A,C,G,T,N,a,c,g,t,n}"
        raise KeyError(error_msg)

def update_mismatches(tag_data, seq, mismatch_list, mismatch_dict, is_reverse):
    """Determine the mismatch types.

    Uses tag values from the 'MD' tag to determine the mismatch types present
    that are above a specified PHRED quality score.

    Args:
        tag_data: A string of the tag value from the 'MD' tag of an alignment.
        seq: A string of the nucleotide sequence for the same alignment that
            corresponds to 'tag_data'.
    """
    current_index = 0
    seq_length = len(seq)
    alphanumeric = re.compile('[A-z]|\d+')
    #Split tag values into the nucleotide and numeric components
    mismatch_code = alphanumeric.findall(tag_data)
    if len(mismatch_code) < 2:
        return
    for item in mismatch_code:
        if item.isdigit():
            current_index += int(item)
        else:
            if is_reverse:
                key = complement(item + seq[current_index])
                mismatch_list[seq_length-current_index-1] += 1
                mismatch_dict[key][seq_length-current_index-1] += 1
            else:
                key = item + seq[current_index]
                mismatch_list[current_index] += 1
                mismatch_dict[key][current_index] += 1
            current_index += 1
    return


def calculate_mismatches(filename, args):
    """
    
    """
    min_quality = args.quality
    min_pct = args.percent
    qlen = args.read_length
    #qlen = peek_length(filename)
    aln_total = 0
    total_mismatches = [0] * qlen
    mismatch_dict = {}
    for mismatch_type in permutations("ACGT", 2):
        mismatch_dict["".join(mismatch_type)] = [0] * qlen
    aln_data = load_aln(filename)
    aln_iter = aln_data.fetch(until_eof=True)
    for aln in aln_iter:
        if aln.is_unmapped or \
            (not meets_qc(aln, min_quality, min_pct/100.0)) or \
            ("N" in aln.get_reference_sequence().upper()):
            continue
        aln_total += 1
        tag_val = aln.get_tag("MD")
        if tag_val == str(qlen):
            continue
        seq = aln.query_alignment_sequence
        update_mismatches(tag_val, seq, total_mismatches, mismatch_dict, aln.is_reverse)
    aln_data.close()
    return [aln_total, total_mismatches, mismatch_dict]


def make_total_mismatch_profile(total_mm):
    """

    """
    mm_vec = np.log10(np.array(total_mm) + 1)
    xvals = range(1,mm_vec.size+1)
    fig = plt.figure(figsize=(12,9))
    ax0 = fig.add_subplot(1,1,1)
    ax0.plot(xvals, mm_vec, 'ok-', label="Total Mismatch Profile", linewidth=3)
    ax0.set_title("Total Mismatches Per Base")
    ax0.set_ylabel("log10(Mismatch Count+1)")
    ax0.set_xlabel("Read Position (5' to 3')")
    return fig

def add_deviation(figure, dev_val):
    assert len(figure.get_axes()) > 0, "Empty figure!"
    ax0 = figure.get_axes()[0]
    ax0.axhline(dev_val, color='r', linestyle=':', label="Deviation Unit")
    return

def make_mismatch_profiles(mm_dict):
    """

    """
    fig = plt.figure(figsize=(13,9))
    ax0 = fig.add_subplot(1,1,1)
    ax0.set_title("Mismatch Profiles")
    ax0.set_ylabel("log10(Mismatch Count+1)")
    ax0.set_xlabel("Read Position (5' to 3')")
    colors = plt.cm.get_cmap("jet", len(mm_dict))
    i=0
    for key in mm_dict.keys():
        mm_vec = np.log10(np.array(mm_dict[key]) + 1)
        xvals = range(1,mm_vec.size+1)
        ax0.plot(xvals, mm_vec, 'o-', c=colors(i), label=key, linewidth=3)
        i+=1
    box = ax0.get_position()
    ax0.set_position([box.x0, box.y0, box.width * 0.9, box.height])
    ax0.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    return fig

def mismatch_rate_boxplot():
    """

    """
    pass

def double_mad(mismatch_list):
    """

    """
    m = np.median(mismatch_list)
    mismatch_vector = np.array(mismatch_list)
    abs_dev = np.abs(mismatch_vector-m)
    left_mad = np.median(abs_dev[np.where(mismatch_vector <= m)])
    right_mad = np.median(abs_dev[np.where(mismatch_vector >= m)])
    if left_mad == 0.0 or right_mad == 0.0:
        raise ValueError, "MAD statistic is 0"
    return m, left_mad, right_mad


def double_mad_filter(mismatch_list, mismatch_dict):
    """

    """
    mismatch_vector = np.array(mismatch_list)
    new_dict = {}
    m, l_mad, r_mad = double_mad(mismatch_list)
    lo = m - (2*l_mad)
    hi = m + (2*r_mad)
    keepers = np.where((mismatch_list >= lo) & (mismatch_list <= hi))
    mismatch_vector = mismatch_vector[keepers]
    for k in mismatch_dict.keys():
        new_dict[k] = np.array(mismatch_dict[k])[keepers]
    return mismatch_vector, new_dict


def join_ambiguous_types(mismatch_dict):
    """

    """
    groups = {1:["AT","TA"],
              2:["CG","GC"],
              3:["GT","CA"],
              4:["TG","AC"],
              5:["CT","GA"],
              6:["TC","AG"]}
    new_dict = {}
    for k in groups.keys():
        m1 = groups[k][0]
        m2 = groups[k][1]
        new_dict[m1] = np.array(mismatch_dict[m1]) + np.array(mismatch_dict[m2])
    return new_dict

    
def mismatch_rate(total_aln, sample_dict):
    new_dict = {}
    for key in sample_dict.keys():
        new_dict[key] = sum(sample_dict[key])/float(total_aln)
    return new_dict


def ttest(groups, sample_data):
    g1 = []
    g2 = []
    i = 0
    labels = list(set(groups))
    for g in groups:
        if g == labels[0]:
            g1.append(sample_data[i])
        else:
            g2.append(sample_data[i])
        i+=1
    test_labels = sample_data[0].keys()
    pvals = {}
    for name in test_labels:
        g1_data = [x[name] for x in g1]
        g2_data = [x[name] for x in g2]
        results = ttest_ind(g1_data, g2_data, equal_var=False)
        #Convert to one-tailed results
        pvals[name] = results[1]/2
    return pvals


def output_sample_info(outfile, data):
    """

    """
    fout = open(outfile, 'w')
    tmp = data[data.keys()[0]]
    full_labels = tmp[3].keys()
    joined_labels = tmp[4].keys()
    header = "SAMPLE\tTOTAL_PASSED_QC\tTOTAL_MISMATCH\t" + "\t".join(full_labels) + "\t"
    header += "\t".join(["AGGREGATED_"+x for x in joined_labels]) + "\t"
    header += "\t".join(["RATE_"+x for x in joined_labels]) + "\n"
    fout.write(header)
    for sample in data.keys():
        outline = [sample]
        total_qc = str(data[sample][1])
        total_mm = str(sum(data[sample][2]))
        outline.append(total_qc)
        outline.append(total_mm)
        for label in full_labels:
            mm_count = str(sum(data[sample][3][label]))
            outline.append(mm_count)
        for label in joined_labels:
            mm_count = str(sum(join_ambiguous_types(data[sample][3])[label]))
            outline.append(mm_count)
        for label in joined_labels:
            mm_count = str(data[sample][4][label])
            outline.append(mm_count)
        fout.write("\t".join(outline) + "\n")
    fout.close()
    return


def main(args):
    sample_info = load_sample_info(args.samples_file)
    num_ids = len(set([x[0] for x in sample_info.values()]))
    groups, data = [], []
    if args.test:
        assert num_ids == 2, "Exactly 2 group IDs must be specified. \
            %d IDs found" % num_ids
    for sample in sample_info.keys():
        mismatch_data = calculate_mismatches(sample, args)
        filtered = double_mad_filter(mismatch_data[1], mismatch_data[2])
        rates = mismatch_rate(mismatch_data[0], 
            join_ambiguous_types(filtered[1]))
        sample_info[sample] += [mismatch_data[0], filtered[0], 
            filtered[1], rates]
        groups.append(sample_info[sample][0])
        data.append(rates)
        fig1 = make_total_mismatch_profile(mismatch_data[1])
        m, l_mad, r_mad = double_mad(mismatch_data[1])
        add_deviation(fig1, np.log10(m-l_mad))
        add_deviation(fig1, np.log10(m+r_mad))
        add_deviation(fig1, np.log10(m-l_mad*2))
        add_deviation(fig1, np.log10(m+r_mad*2))
        fig2 = make_mismatch_profiles(mismatch_data[2])
        fig1_name = args.out_dir + "/" + basename(splitext(sample)[0]) + "_total_mm.pdf"
        fig2_name = args.out_dir + "/" + basename(splitext(sample)[0]) + "_individual_mm.pdf"
        fig1.savefig(fig1_name)
        fig2.savefig(fig2_name)
        plt.close(fig1)
        plt.close(fig2)
    if args.test:
        labels, pvals = zip(*ttest(groups, data).items())
        qvals = multipletests(pvals, alpha=0.05, method='fdr_by')[1]
        print "Labels:\t" + "\t".join(labels)
        print "p-values:\t" + "\t".join([str(x) for x in pvals])
        print "q-values:\t" + "\t".join([str(x) for x in qvals]) 
    outfile = args.out_dir + "/" + basename(splitext(args.samples_file)[0]) + ".out"
    output_sample_info(outfile, sample_info)


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser(prog='Read Mismatch Classifier',
        usage='python mismatch_classifier.py [options] samples_file',
        description='Compute statistics related to read mismatches.')
    parser.add_argument('samples_file',
        type=str,
        help='Tab-delimited file with first column containing SAM/BAM file \
            paths and the second column containing group IDs. Exactly two \
            group IDs must be specified if --test is specified.')
    parser.add_argument('-o', '--out-dir',
        default='.',
        type=str,
        help='Path for file output.')
    parser.add_argument('-q', '--quality',
        default=20,
        type=int,
        help='Minimum PHRED score value used in filtering. DEFAULT: 20')
    parser.add_argument('-p', '--percent',
        default=90,
        type=int,
        help='Percentage of bases in a read that must meet or exceed \
            --quality for the read to be retained. DEFAULT: 90')
    parser.add_argument('-l','--read-length',
        type=int,
        help="Length of the reads in the sample.")
    parser.add_argument('-t', '--test',
        action='store_true',
        help='If specified, t-tests are conducted between groups for each \
            mismatch class and FDR correction is applied.')
    parser.add_argument('-V', '--version',
        action='version',
        version='%(prog)s 1.0',
        help='Display the program version')
    args = parser.parse_args()

    if not exists(args.out_dir):
        makedirs(args.out_dir)
    if args.read_length is None:
        raise ValueError, "Please specify the read length with -l or --read-length"
    main(args)

