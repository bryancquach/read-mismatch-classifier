#!/usr/bin/env python

"""
Author: Bryan Quach
"""

from toolbox import Database, Calculator
from os import makedirs
from os.path import exists, split, basename
import numpy as np
import pysam
import re


def main(args):

    calculator = Calculator(Database(args.aln_file, args.read_length))
    calculator.compute_mm()
    total_mm = "\t".join([str(x) for x in calculator.get_total_mm_vector()]) + "\n"
    filename = args.out_dir + "/" + basename(args.aln_file).split(".")[0] + ".totalmm.txt"
    fout = open(filename, 'w')
    fout.write(total_mm)
    fout.close()
    return

if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser(prog='Total Mismatch Calculator',
        usage='python get_total_mm_profile.py [options] aln_file read_length',
        description='Tabulate total per base read mismatchs.')
    parser.add_argument('aln_file',
        type=str,
        help='SAM/BAM file with MD and NM tags that follow the format used by '
            'SAMTools calmd.')
    parser.add_argument('read_length',
        type=int,
        help="Length of the reads in the sample.")
    parser.add_argument('-o', '--out-dir',
        default='.',
        type=str,
        help='Path for output. DEFAULT: working directory')
    args = parser.parse_args()

    if not exists(args.out_dir):
        makedirs(args.out_dir)
    main(args)

