
# Analyzing mismatches in sequencing reads

__Author:__ Bryan Quach  
__Date created:__ June 7, 2017

This notebook serves as tutorial for how to process a BAM/SAM file from aligned RNA-seq or ATAC-seq reads to produce mismatch statistics and visualizations. It also serves as a log that lays out the steps taken in the analysis of a particular data set.

## Motivation and Purpose

When aligning reads to a reference genome, the parameters of the aligner may allow for alignments with mismatches. These mismatches may be informative for various purposes such as diagnosing position specific quality issues in reads, identifying biases, etc. In addition to general reasons why calculating mismatch statistics may be informative, the Furey Lab specifically aims to assess if the rate of specific types of mutations differ between a treatment and control group of mice. The treatment group mice possess genomes that have accumulated DNA adducts, which can cause mutations during DNA damage repair. Consequently, \*-seq experiments performed on treatment group samples may include an increased number of reads that do not perfectly align to their originating location on the reference genome when compared to control group samples. Our assumption is that read alignment mismatches can serve as a (noisy) proxy for investigating rates of DNA transversion and transition mutations. Our working hypothesis is that treatment group mice have higher rates of DNA mutations than control mice.

## Software and tools

__Note:__ This analysis may work with other versions of these software and tools, but we cannot guarantee compatibility and correct behavior with other versions.

* [BEDTools v2.25.0](http://bedtools.readthedocs.io/en/latest/)
* [SAMTools v1.3.1](http://www.htslib.org/)
* [RSEM v1.2.31](http://deweylab.github.io/RSEM/)
* [Pysam v0.9.1.4](http://pysam.readthedocs.io/en/latest/) (Python Package)
* [Matplotlib v1.5.0](http://matplotlib.org/) (Python Package)
* [Numpy v1.8.0](http://www.numpy.org/) (Python Package)
* [Scipy v0.15.1](http://www.scipy.org/) (Python Package)
* [Statsmodels v0.6.1](http://statsmodels.sourceforge.net/) (Python Package)
* Python v2.7.6
* Linux/Unix-like environment
* LSF (or SLURM) job scheduler and workload management platform. If using SLURM, code modifications will need to be made.

## Analysis Outline

__Note:__ If you have your own data that has already been pre-processed, then skip to step 4.

1. [Data sources](#Data-Sources)
2. [ATAC-seq data initial processing](#ATAC-seq-data-initial-processing)
3. [RNA-seq data initial processing](#RNA-seq-data-initial-processing)
4. [Read filtering](#Read-filtering)
5. [Calculating per base mismatch counts](#Calculating-per-base-mismatch-counts)
6. [Plotting per base mismatch profiles](#Plotting-per-base-mismatch-profiles)
7. [Comparing mismatch rates between sample groups](#Comparing-mismatch-rates-between-sample-groups)

## Data Sources

The samples for which we have data are from liver, lung, and kidney tissue of C57BL6 and CAST mice. The treatment group mice were exposed to 1,3-butadiene gas. The control group mice were not. RNA-seq and ATAC-seq were performed on these samples. Both the RNA-seq and ATAC-seq data are accessible on the research computing disk space via Longleaf or Killdevil. After logging on to either server, these can be viewed by using the following linux commands:


```bash
#List RNA-seq samples
ls /proj/fureylab/data/RNA-seq/mouse/{C57BL6,CAST}/{BD,CTL}*/BD_RNAtotal*/*transcript.bam

#List ATAC-seq samples
ls /proj/fureylab/data/ATAC/mouse/{C57BL6,CAST}/{BD,CTL}*/*/out/blBD_ATAC*.sorted.bam
```

To avoid disk usage and read/write permission issues, copy all files to the server scratch space and put them in a project folder called `mutation_analysis`. On Killdevil the scratch space is under `/netscr`. On Longleaf the scratch space is under `/pine/scr/a/b` where `a` and `b` are your first and last initials respectively. The following instructions will be based on using the scratch space for my ONYEN. Modify the code accordingly to fit your directory setup. It is also important to note that the job submission commands are for LSF and not SLURM.

**We will be excluding the following samples from downstream analysis:**

* RNA-seq samples: CAST BD-exposed lung #8
* ATAC-seq samples: CAST control kidney #2 and BD-exposed kidney #7


```bash
#Create project folder
cd /netscr/bquach/
mkdir mutation_analysis
mkdir -p mutation_analysis/c57bl6/{atac,rna}
mkdir -p mutation_analysis/cast/{atac,rna}

#Distribute copy jobs on the cluster
cd mutation_analysis
for i in /proj/fureylab/data/RNA-seq/mouse/C57BL6/{BD,CTL}*/BD_RNAtotal*/RNA*transcript.bam; do bsub -q hour -M 1 "cp ${i} c57bl6/rna"; done  
for i in /proj/fureylab/data/RNA-seq/mouse/CAST/{BD,CTL}*/BD_RNAtotal*/RNA*transcript.bam; do bsub -q hour -M 1 "cp ${i} cast/rna"; done  
for i in /proj/fureylab/data/ATAC/mouse/C57BL6/[BD\|CTL]*/*/out/blBD_ATAC*.sorted.bam; do bsub -q hour -M 1 "cp ${i} c57bl6/atac"; done  
for i in /proj/fureylab/data/ATAC/mouse/CAST/[BD\|CTL]*/*/out/blBD_ATAC*.sorted.bam; do bsub -q hour -M 1 "cp ${i} cast/atac"; done

#Copy and modify unconventional filenames for liver RNA samples
for i in /proj/fureylab/data/RNA-seq/mouse/C57BL6/{BD,CTL}*/BD_RNAtotal*/BD*transcript.bam; 
do 
    bsub -q hour -M 1 "cp $i $(echo $i | awk -F / '{OFS="-"} {print "c57bl6/rna/"$9".transcript.bam"}')"; 
done
for i in /proj/fureylab/data/RNA-seq/mouse/CAST/{BD,CTL}*/BD_RNAtotal*/BD*transcript.bam; 
do 
    bsub -q hour -M 1 "cp $i $(echo $i | awk -F / '{OFS="-"} {print "cast/rna/"$9".transcript.bam"}')"; 
done


#Remove problematic samples
rm cast/rna/RNATOTLU8-BD_RNAtotal_8_lung_ACTGAT_L001_R1_001_RNATOTLU8-BD_RNAtotal_8_lung_ACTGAT_L001_R2_001.transcript.bam
rm cast/atac/blBD_ATAC_2_kidney.sorted.bam
rm cast/atac/blBD_ATAC_7_kidney.sorted.bam
```

## ATAC-seq data initial processing

From the ATAC-seq BAM files, we need to retrieve only the reads that align to peak regions. For our analysis, we have peak calls in narrow peak format (NPF) from [F-Seq](http://fureylab.web.unc.edu/software/fseq/) that we can use in conjuction with BEDTools to grab the desired alignments.


```bash
#List F-Seq peak call files
ls /proj/fureylab/data/ATAC/mouse/{C57BL6,CAST}/{CTL,BD}*/BD_ATAC_*/out/*npf

# From these NPF files we want to extract the top 50,000 peaks ranked by signal enrichment and store them in new files. 
# As a quality control pre-caution, we remove any potential peaks that contain any overlap with a blacklisted genomic region.
module load bedtools/2.25.0 
cd /netscr/bquach/mutation_analysis

#Blacklist filter and extract top peaks for C57BL6
for i in /proj/fureylab/data/ATAC/mouse/C57BL6/{CTL,BD}*/BD_ATAC_*/out/*npf;
do 
    bsub -q hour -M 4 "intersectBed -v -a ${i} -b /proj/fureylab/genomes/mouse/Blacklist/mm9_blacklist_raw_v2_3col_sorted_merged500bp.bed | sort -nrk7,7 | head -50000 > c57bl6/atac/$(basename ${i/.npf/.top50k.npf})";
done

#Blacklist filter and extract top peaks for CAST
for i in /proj/fureylab/data/ATAC/mouse/CAST/{CTL,BD}*/BD_ATAC_*/out/*npf; 
do 
    bsub -q hour -M 4 "intersectBed -v -a ${i} -b /proj/fureylab/genomes/mouse/Blacklist/CAST_blacklist_raw_3col_sorted_merged500bp.bed | sort -nrk7,7 | head -50000 > cast/atac/$(basename ${i/.npf/.top50k.npf})";
done

# To aid in batch processing of the BAM files we will standardize the file prefixes of the NPF and BAM files for each sample. 
# In this analysis, we have NPF filenames that follow the prefix naming conventions 
# ATAC_B6_BD_[tissue]_[ID].top50k, BD_ATAC_[ID]_[tissue].top50k, and ssBD_ATAC_[ID]_[tissue].top50k but we want them to be in the format 
# blBD_ATAC_[ID]_[tissue].top50k
for i in c57bl6/atac/ATAC*npf; do mv $i $(echo $i | cut -d'.' -f1,1 | awk -F / '{OFS="/"} {print $1,$2,"blBD_"$3}' | awk -F _ '{OFS="_"} {print $1,$2,$6,$5".top50k.npf"}'); done
for i in c57bl6/atac/BD*npf; do mv $i $(echo $i | awk -F / '{OFS="/"} {print $1,$2,"bl"$3}'); done
for i in cast/atac/ssBD*npf; do mv $i ${i/ssBD/blBD}; done

#Check that all the files should have the same name prefix
for i in */atac/*; do basename $i; done
```

With the files standardized, we can now extract the reads aligning to peak regions using distributed processing. Once the new files are generated, we use the SAMTools `calmd` command to produce BAM files with MD tags. This ensures that all the BAM files are following the same MD tag syntax. Our files are already sorted, but if they were not, it is *strongly* recommended to sort them first using `samtools sort`. This significantly improves the speed of `calmd`.


```bash
module load samtools/1.3.1
for i in $(ls */atac/*bam | cut -f1,1 -d'.'); do bsub -q hour "intersectBed -abam ${i}.sorted.bam -b ${i}.top50k.npf > ${i}.top50k.sorted.bam"; done

# Wait for previous jobs to finish before running
for i in c57bl6/atac/*top50k.sorted.bam; do bsub -q hour "samtools calmd -b ${i} /proj/fureylab/genomes/mouse/mm9_reference/mm9.fa > ${i/sorted.bam/sorted.md.bam}"; done
for i in cast/atac/*top50k.sorted.bam; do bsub -q hour "samtools calmd -b ${i} /proj/fureylab/genomes/mouse/CAST/CASTEiJ_b37_f.fa > ${i/sorted.bam/sorted.md.bam}"; done 
```

The BAM files output from `calmd` should now be in the correct format for [read filtering](#Read-filtering).

## RNA-seq data initial processing

Our RNA-seq BAM files were aligned to reference transcriptomes, so we need to convert them back to genome coordinate space for further processing (SAMTools `calmd` requires files in genome coordinate space). RSEM has a convenient tool for converting transcript BAM files into genome BAM files using [`rsem-tbam2gbam`](http://deweylab.github.io/RSEM/README.html#aconvertingtranscriptbamfileintogenomebamfile). After cleaning up the filenames we apply `rsem-tbam2gbam`.

**Note:** In converting transcriptome BAM files to genome BAM files with rsem-tbam2gbam the total number of reads in the genome file is reduced. This could be due to several factors, but I am not sure why this happens. Documentation from RSEM does not provide any answers.


```bash
module load rsem/1.2.31
module load bedtools/2.25.0
module load samtools/1.3.1

cd /netscr/bquach/mutation_analysis

#Clean up BAM filenames
for i in c57bl6/rna/RNA*transcript.bam; do mv $i $(echo $i | awk -F - '{OFS="-"} {print "c57bl6/rna/"$3}'); done
for i in cast/rna/RNA*transcript.bam; do mv $i $(echo $i | awk -F - '{OFS="-"} {print "cast/rna/"$3}'); done

#Apply rsem-tbam2gbam
for i in c57bl6/rna/*transcript.bam; do
    bsub -q hour -n 4 -R "span[hosts=1]" "rsem-tbam2gbam /proj/fureylab/genomes/mouse/mm9_rsem/rsem_refseq_jw ${i} ${i/transcript.bam/genome.bam} -p 4";
done
for i in cast/rna/*transcript.bam; do
    bsub -q hour -n 4 -R "span[hosts=1]" "rsem-tbam2gbam /proj/fureylab/genomes/mouse/CAST_rsem/rsem_refseq_CAST ${i} ${i/transcript.bam/genome.bam} -p 4";
done
```

As a quality control pre-caution, we apply blacklist filtering. Following this we sort the results using `samtools sort` and input this into the SAMTools `calmd` command to produce BAM files with MD tags. This ensures that all the BAM files are following the same MD tag syntax. The sorting step significantly improves the speed of `calmd`.


```bash
#Blacklist filtering
for i in c57bl6/rna/*genome.bam; do bsub -q hour "intersectBed -v -abam ${i} -b /proj/fureylab/genomes/mouse/Blacklist/mm9_blacklist_raw_v2_3col_sorted_merged500bp.bed > ${i/genome.bam/genome.bl.bam}"; done
for i in cast/rna/*genome.bam; do bsub -q hour "intersectBed -v -abam ${i} -b /proj/fureylab/genomes/mouse/Blacklist/CAST_blacklist_raw_3col_sorted_merged500bp.bed > ${i/genome.bam/genome.bl.bam}"; done

#Sort files
for i in */rna/*genome.bl.bam; do bsub -q hour "samtools sort ${i} -o ${i/bl.bam/bl.sorted.bam}"; done

#Apply calmd
for i in c57bl6/rna/*genome.bl.sorted.bam; do bsub -q hour "samtools calmd -b ${i} /proj/fureylab/genomes/mouse/mm9_reference/mm9.fa > ${i/genome.bl/genome.bl.md}"; done
for i in cast/rna/*genome.bl.sorted.bam; do bsub -q hour "samtools calmd -b ${i} /proj/fureylab/genomes/mouse/CAST/CASTEiJ_b37_f.fa > ${i/genome.bl/genome.bl.md}"; done 
```

The BAM files output from `calmd` should now be in the correct format for [read filtering](#Read-filtering).

## Read filtering

To remove alignments that would be problematic for calculating mismatch statistics, we apply several alignment filtering criteria. These criteria are:

* Alignments must be on a whitelist of references
* Alignments meet default QC checks of the sequencing platform/vendor
* Alignments must meet specified MAPQ score
* Alignments must meet minimum PHRED quality score for specified fraction of bases
* Alignments cannot contain indels
* Alignments must be primary
* Alignments cannot be a duplicate
* Alignments must be mapped
* Alignments cannot be hard or soft-clipped
* Read and reference sequences of alignments cannot contain 'N' nucleotides 

All of these filtering steps have been implemented in the Python script `bam_filter.py`. This script accepts either BAM or SAM files, so if you have alignment files that contain `calmd` formatted `MD` and `NM` tags and meet the quality control standards outlined in the initial data processing steps, you can use those files directly with `bam_filter.py`. For our purposes, the whitelist acts as a way to exclude alignments to nonconventional references such as *chrM* or *chr1_random*. We use default values for all other arguments (`--phred 20 --percent 90 --mapq 30`). 

If `pysam` is not installed then you will need to install it first. If [pip](https://pip.pypa.io/en/stable/installing/) is installed then you can install `pysam` by using


```bash
pip install pysam --user
```


```bash
#Retrieve scripts from git repo
cd /netscr/bquach/mutation_analysis
git clone https://bryancquach@bitbucket.org/bryancquach/read-mismatch-classifier.git #only do if not yet cloned
git pull #do this if cloned but not updated

#Build whitelist
echo -e "chr1\nchr2\nchr3\nchr4\nchr5\nchr6\nchr7\nchr8\nchr9\nchr10\nchr11\nchr12\nchr13\nchr14\nchr15\nchr16\nchr17\nchr18\nchr19\nchrX\nchrY" > whitelist.txt

#Apply filtering
for i in */rna/*genome.bl.md.sorted.bam; do 
    bsub -q hour "python read-mismatch-classifier/mismatch_classifier/bam_filter.py ${i} ${i/genome.bl.md.sorted.bam/final.bam} whitelist.txt"; 
done

for i in */atac/*sorted.md.bam; do 
    bsub -q hour "python read-mismatch-classifier/mismatch_classifier/bam_filter.py ${i} ${i/sorted.md.bam/final.bam} whitelist.txt"; 
done
```

## Calculating initial total per base mismatch counts

With the filtered BAM files, we can use `get_total_mm_profile.py` to generate total per base mismatches. This script will produce a tab-delimited text file containing the mismatches for every position in a read. As such, the read length of the sample must be specified. In our case the RNA-seq samples have read lengths of 48 and the ATAC-seq samples have read lengths of 50.

The Python packages `numpy` and `scipy` will need to be installed if they haven't already.


```bash
pip install numpy --user
pip install scipy --user
```


```bash
#Show usage information
python read-mismatch-classifier/mismatch_classifier/get_total_mm_profile.py --help

#Calculate mismatches for RNA-seq samples
for i in c57bl6/rna/*final.bam; do 
    bsub -q hour "python read-mismatch-classifier/mismatch_classifier/get_total_mm_profile.py -o c57bl6/rna/ ${i} 48"; 
done
for i in cast/rna/*final.bam; do 
    bsub -q hour "python read-mismatch-classifier/mismatch_classifier/get_total_mm_profile.py -o cast/rna/ ${i} 48"; 
done

#Calculate mismatches for ATAC-seq samples
for i in c57bl6/atac/*final.bam; do 
    bsub -q hour "python read-mismatch-classifier/mismatch_classifier/get_total_mm_profile.py -o c57bl6/atac/ ${i} 50"; 
done
for i in cast/atac/*final.bam; do 
    bsub -q hour "python read-mismatch-classifier/mismatch_classifier/get_total_mm_profile.py -o cast/atac/ ${i} 50"; 
done

```

## Aggregating total mismatches into an R matrix

For exploratory analyses and other downstream analyses in R, code is provided for creating an R matrix that holds the total mismatches per base for each sample within a directory. The script to do this is `get_total_mm_matrix.R`. The matrix is stored as an RDS file that can be loaded in R using `readRDS()`. This functions similarly to `load()` for RData objects, excepts RDS objects can be assigned to a variable name of choice.


```bash
bsub -q hour -M 1 "Rscript read-mismatch-classifier/mismatch_classifier/get_total_mm_matrix.R c57bl6/rna/ c57bl6_rna_total_mm_matrix.rds"
bsub -q hour -M 1 "Rscript read-mismatch-classifier/mismatch_classifier/get_total_mm_matrix.R cast/rna/ cast_rna_total_mm_matrix.rds"
bsub -q hour -M 1 "Rscript read-mismatch-classifier/mismatch_classifier/get_total_mm_matrix.R c57bl6/atac/ c57bl6_atac_total_mm_matrix.rds"
bsub -q hour -M 1 "Rscript read-mismatch-classifier/mismatch_classifier/get_total_mm_matrix.R cast/atac/ cast_atac_total_mm_matrix.rds"
```

## Detecting outlier bases (preliminary)

Due to sequencing error, some read positions have an unexpectedly high or low number of mismatches. To accomodate for this we apply a simple outlier detection method that assumes that within and across samples, most read positions have a consistent mismatch rate if you account for differences due to sequencing depth. The outlier detection approach we take first median-centers the mismatch count data for each sample then uses an aggregate distribution of normalized mismatch counts to determine outliers. An R code example of how this can be done is shown below.


```bash
c57 = readRDS(file="c57bl6_rna_total_mm_matrix.rds")
cast = readRDS(file="cast_rna_total_mm_matrix.rds")
rna = rbind(c57,cast)
med.total = apply(t(rna), 2, median)
med.total_centered = sweep(data.matrix(t(rna)), 2, med.total)
#boxplot(med.total_centered)
test = as.vector(med.total_centered)
test.stats = boxplot(test)
test.stats$stats #hinge values based on full distribution, mean-centered data
#obtain median for each sample (normal scale, not centered) and find lower and upper value (-/+ hinge values from test.stats$stats):
median = as.data.frame(med.total)
median$lower = median$med.total - 1981.25
median$upper = median$med.total + 6871.00
write.table(median, file="MultiTissue_Mutation_Median_Hinge.txt", sep="\t", quote=FALSE)
```

## Calculating mismatch rates

To generate a text file of mismatch rates for each sample, we can use the script `get_mm_rates.py`. The script provides the option to include or exclude a file with mismatch count cutoffs for determining outliers. The code below is shown for RNA-seq data only.


```bash
for i in *c57bl6/rna/*final.bam; do 
    bsub -q hour "python read-mismatch-classifier/mismatch_classifier/get_mm_rates.py -o c57bl6/rna/ -t MultiTissue_Mutation_Median_Hinge.txt ${i} 48";  
done
for i in *cast/rna/*final.bam; do 
    bsub -q hour "python read-mismatch-classifier/mismatch_classifier/get_mm_rates.py -o cast/rna/ -t MultiTissue_Mutation_Median_Hinge.txt ${i} 48";  
done
```

## Calculating grouped mismatch rates

To generate a text file of grouped mismatch rates for each sample, we can use the script `get_grouped_mm_rates.py`. The script provides the option to include or exclude a file with mismatch count cutoffs for determining outliers. The code below is shown for RNA-seq data only.


```bash
for i in *c57bl6/rna/*final.bam; do 
    bsub -q hour "python read-mismatch-classifier/mismatch_classifier/get_grouped_mm_rates.py -o c57bl6/rna/ -t MultiTissue_Mutation_Median_Hinge.txt ${i} 48";  
done
for i in *cast/rna/*final.bam; do 
    bsub -q hour "python read-mismatch-classifier/mismatch_classifier/get_grouped_mm_rates.py -o cast/rna/ -t MultiTissue_Mutation_Median_Hinge.txt ${i} 48";  
done
```

## Aggregating mismatch rates into an R matrix

For exploratory analyses and other downstream analyses in R, code is provided for creating an R matrix that holds the mismatch rate per mutation type for each sample within a directory. The script to do this is `get_mm_rates_matrix.R`. The matrix is stored as an RDS file that can be loaded in R using `readRDS()`. This functions similarly to `load()` for RData objects, excepts RDS objects can be assigned to a variable name of choice.


```bash
# For individual mm types
bsub -q hour -M 1 "Rscript read-mismatch-classifier/mismatch_classifier/get_mm_rate_matrix.R c57bl6/rna/ c57bl6_rna_mm_rates_matrix.rds"
bsub -q hour -M 1 "Rscript read-mismatch-classifier/mismatch_classifier/get_mm_rate_matrix.R cast/rna/ cast_rna_mm_rates_matrix.rds"

# For grouped mm types
bsub -q hour -M 1 "Rscript read-mismatch-classifier/mismatch_classifier/get_grouped_mm_rate_matrix.R c57bl6/rna/ c57bl6_rna_grouped_mm_rates_matrix.rds"
bsub -q hour -M 1 "Rscript read-mismatch-classifier/mismatch_classifier/get_grouped_mm_rate_matrix.R cast/rna/ cast_rna_grouped_mm_rates_matrix.rds"

```

## Plotting per base mismatch profiles (incomplete)

## Comparing mismatch rates between sample groups (incomplete)

To generate the list of samples for each strain and tissue, we used the code below. Because of inconsistent naming, we take into account the possibiliy of the tissue label being either capitalized or lowercase.


```bash
for j in {Kidney,Liver,Lung}; do 
    for i in c57bl6/rna/*["${j}|$(echo ${j} | tr A-Z a-z)"]*pkl; do 
        echo -e "${i}\t1" >> c57bl6_rna_$(echo ${j} | tr A-Z a-z).txt; 
    done; 
done
for j in {Kidney,Liver,Lung}; do 
    for i in cast/rna/*["${j}|$(echo ${j} | tr A-Z a-z)"]*pkl; do 
        echo -e "${i}\t1" >> cast_rna_$(echo ${j} | tr A-Z a-z).txt; 
    done; 
done
for j in {Kidney,Liver,Lung}; do 
    for i in c57bl6/atac/*["${j}|$(echo ${j} | tr A-Z a-z)"]*pkl; do 
        echo -e "${i}\t1" >> c57bl6_atac_$(echo ${j} | tr A-Z a-z).txt; 
    done; 
done
for j in {Kidney,Liver,Lung}; do 
    for i in cast/atac/*["${j}|$(echo ${j} | tr A-Z a-z)"]*pkl; do 
        echo -e "${i}\t1" >> cast_atac_$(echo ${j} | tr A-Z a-z).txt; 
    done; 
done
```


```bash

```
