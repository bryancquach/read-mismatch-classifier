{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "# Analyzing mismatches in sequencing reads\n",
    "\n",
    "__Author:__ Bryan Quach  \n",
    "__Date created:__ June 7, 2017\n",
    "\n",
    "This notebook serves as tutorial for how to process a BAM/SAM file from aligned RNA-seq or ATAC-seq reads to produce mismatch statistics and visualizations. It also serves as a log that lays out the steps taken in the analysis of a particular data set.\n",
    "\n",
    "## Motivation and Purpose\n",
    "\n",
    "When aligning reads to a reference genome, the parameters of the aligner may allow for alignments with mismatches. These mismatches may be informative for various purposes such as diagnosing position specific quality issues in reads, identifying biases, etc. In addition to general reasons why calculating mismatch statistics may be informative, the Furey Lab specifically aims to assess if the rate of specific types of mutations differ between a treatment and control group of mice. The treatment group mice possess genomes that have accumulated DNA adducts, which can cause mutations during DNA damage repair. Consequently, \\*-seq experiments performed on treatment group samples may include an increased number of reads that do not perfectly align to their originating location on the reference genome when compared to control group samples. Our assumption is that read alignment mismatches can serve as a (noisy) proxy for investigating rates of DNA transversion and transition mutations. Our working hypothesis is that treatment group mice have higher rates of DNA mutations than control mice.\n",
    "\n",
    "## Software and tools\n",
    "\n",
    "__Note:__ This analysis may work with other versions of these software and tools, but we cannot guarantee compatibility and correct behavior with other versions.\n",
    "\n",
    "* [BEDTools v2.25.0](http://bedtools.readthedocs.io/en/latest/)\n",
    "* [SAMTools v1.3.1](http://www.htslib.org/)\n",
    "* [RSEM v1.2.31](http://deweylab.github.io/RSEM/)\n",
    "* [Pysam v0.9.1.4](http://pysam.readthedocs.io/en/latest/) (Python Package)\n",
    "* [Matplotlib v1.5.0](http://matplotlib.org/) (Python Package)\n",
    "* [Numpy v1.8.0](http://www.numpy.org/) (Python Package)\n",
    "* [Scipy v0.15.1](http://www.scipy.org/) (Python Package)\n",
    "* [Statsmodels v0.6.1](http://statsmodels.sourceforge.net/) (Python Package)\n",
    "* Python v2.7.6\n",
    "* Linux/Unix-like environment\n",
    "* LSF (or SLURM) job scheduler and workload management platform. If using SLURM, code modifications will need to be made.\n",
    "\n",
    "## Analysis Outline\n",
    "\n",
    "__Note:__ If you have your own data that has already been pre-processed, then skip to step 4.\n",
    "\n",
    "1. [Data sources](#Data-Sources)\n",
    "2. [ATAC-seq data initial processing](#ATAC-seq-data-initial-processing)\n",
    "3. [RNA-seq data initial processing](#RNA-seq-data-initial-processing)\n",
    "4. [Read filtering](#Read-filtering)\n",
    "5. [Calculating per base mismatch counts](#Calculating-per-base-mismatch-counts)\n",
    "6. [Plotting per base mismatch profiles](#Plotting-per-base-mismatch-profiles)\n",
    "7. [Comparing mismatch rates between sample groups](#Comparing-mismatch-rates-between-sample-groups)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "## Data Sources\n",
    "\n",
    "The samples for which we have data are from liver, lung, and kidney tissue of C57BL6 and CAST mice. The treatment group mice were exposed to 1,3-butadiene gas. The control group mice were not. RNA-seq and ATAC-seq were performed on these samples. Both the RNA-seq and ATAC-seq data are accessible on the research computing disk space via Longleaf or Killdevil. After logging on to either server, these can be viewed by using the following linux commands:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "#List RNA-seq samples\n",
    "ls /proj/fureylab/data/RNA-seq/mouse/{C57BL6,CAST}/{BD,CTL}*/BD_RNAtotal*/*transcript.bam\n",
    "\n",
    "#List ATAC-seq samples\n",
    "ls /proj/fureylab/data/ATAC/mouse/{C57BL6,CAST}/{BD,CTL}*/*/out/blBD_ATAC*.sorted.bam"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "To avoid disk usage and read/write permission issues, copy all files to the server scratch space and put them in a project folder called `mutation_analysis`. On Killdevil the scratch space is under `/netscr`. On Longleaf the scratch space is under `/pine/scr/a/b` where `a` and `b` are your first and last initials respectively. The following instructions will be based on using the scratch space for my ONYEN. Modify the code accordingly to fit your directory setup. It is also important to note that the job submission commands are for LSF and not SLURM.\n",
    "\n",
    "**We will be excluding the following samples from downstream analysis:**\n",
    "\n",
    "* RNA-seq samples: CAST BD-exposed lung #8\n",
    "* ATAC-seq samples: CAST control kidney #2 and BD-exposed kidney #7"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "#Create project folder\n",
    "cd /netscr/bquach/\n",
    "mkdir mutation_analysis\n",
    "mkdir -p mutation_analysis/c57bl6/{atac,rna}\n",
    "mkdir -p mutation_analysis/cast/{atac,rna}\n",
    "\n",
    "#Distribute copy jobs on the cluster\n",
    "cd mutation_analysis\n",
    "for i in /proj/fureylab/data/RNA-seq/mouse/C57BL6/{BD,CTL}*/BD_RNAtotal*/RNA*transcript.bam; do bsub -q hour -M 1 \"cp ${i} c57bl6/rna\"; done  \n",
    "for i in /proj/fureylab/data/RNA-seq/mouse/CAST/{BD,CTL}*/BD_RNAtotal*/RNA*transcript.bam; do bsub -q hour -M 1 \"cp ${i} cast/rna\"; done  \n",
    "for i in /proj/fureylab/data/ATAC/mouse/C57BL6/[BD\\|CTL]*/*/out/blBD_ATAC*.sorted.bam; do bsub -q hour -M 1 \"cp ${i} c57bl6/atac\"; done  \n",
    "for i in /proj/fureylab/data/ATAC/mouse/CAST/[BD\\|CTL]*/*/out/blBD_ATAC*.sorted.bam; do bsub -q hour -M 1 \"cp ${i} cast/atac\"; done\n",
    "\n",
    "#Copy and modify unconventional filenames for liver RNA samples\n",
    "for i in /proj/fureylab/data/RNA-seq/mouse/C57BL6/{BD,CTL}*/BD_RNAtotal*/BD*transcript.bam; \n",
    "do \n",
    "    bsub -q hour -M 1 \"cp $i $(echo $i | awk -F / '{OFS=\"-\"} {print \"c57bl6/rna/\"$9\".transcript.bam\"}')\"; \n",
    "done\n",
    "for i in /proj/fureylab/data/RNA-seq/mouse/CAST/{BD,CTL}*/BD_RNAtotal*/BD*transcript.bam; \n",
    "do \n",
    "    bsub -q hour -M 1 \"cp $i $(echo $i | awk -F / '{OFS=\"-\"} {print \"cast/rna/\"$9\".transcript.bam\"}')\"; \n",
    "done\n",
    "\n",
    "\n",
    "#Remove problematic samples\n",
    "rm cast/rna/RNATOTLU8-BD_RNAtotal_8_lung_ACTGAT_L001_R1_001_RNATOTLU8-BD_RNAtotal_8_lung_ACTGAT_L001_R2_001.transcript.bam\n",
    "rm cast/atac/blBD_ATAC_2_kidney.sorted.bam\n",
    "rm cast/atac/blBD_ATAC_7_kidney.sorted.bam"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "## ATAC-seq data initial processing\n",
    "\n",
    "From the ATAC-seq BAM files, we need to retrieve only the reads that align to peak regions. For our analysis, we have peak calls in narrow peak format (NPF) from [F-Seq](http://fureylab.web.unc.edu/software/fseq/) that we can use in conjuction with BEDTools to grab the desired alignments."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "#List F-Seq peak call files\n",
    "ls /proj/fureylab/data/ATAC/mouse/{C57BL6,CAST}/{CTL,BD}*/BD_ATAC_*/out/*npf\n",
    "\n",
    "# From these NPF files we want to extract the top 50,000 peaks ranked by signal enrichment and store them in new files. \n",
    "# As a quality control pre-caution, we remove any potential peaks that contain any overlap with a blacklisted genomic region.\n",
    "module load bedtools/2.25.0 \n",
    "cd /netscr/bquach/mutation_analysis\n",
    "\n",
    "#Blacklist filter and extract top peaks for C57BL6\n",
    "for i in /proj/fureylab/data/ATAC/mouse/C57BL6/{CTL,BD}*/BD_ATAC_*/out/*npf;\n",
    "do \n",
    "    bsub -q hour -M 4 \"intersectBed -v -a ${i} -b /proj/fureylab/genomes/mouse/Blacklist/mm9_blacklist_raw_v2_3col_sorted_merged500bp.bed | sort -nrk7,7 | head -50000 > c57bl6/atac/$(basename ${i/.npf/.top50k.npf})\";\n",
    "done\n",
    "\n",
    "#Blacklist filter and extract top peaks for CAST\n",
    "for i in /proj/fureylab/data/ATAC/mouse/CAST/{CTL,BD}*/BD_ATAC_*/out/*npf; \n",
    "do \n",
    "    bsub -q hour -M 4 \"intersectBed -v -a ${i} -b /proj/fureylab/genomes/mouse/Blacklist/CAST_blacklist_raw_3col_sorted_merged500bp.bed | sort -nrk7,7 | head -50000 > cast/atac/$(basename ${i/.npf/.top50k.npf})\";\n",
    "done\n",
    "\n",
    "# To aid in batch processing of the BAM files we will standardize the file prefixes of the NPF and BAM files for each sample. \n",
    "# In this analysis, we have NPF filenames that follow the prefix naming conventions \n",
    "# ATAC_B6_BD_[tissue]_[ID].top50k, BD_ATAC_[ID]_[tissue].top50k, and ssBD_ATAC_[ID]_[tissue].top50k but we want them to be in the format \n",
    "# blBD_ATAC_[ID]_[tissue].top50k\n",
    "for i in c57bl6/atac/ATAC*npf; do mv $i $(echo $i | cut -d'.' -f1,1 | awk -F / '{OFS=\"/\"} {print $1,$2,\"blBD_\"$3}' | awk -F _ '{OFS=\"_\"} {print $1,$2,$6,$5\".top50k.npf\"}'); done\n",
    "for i in c57bl6/atac/BD*npf; do mv $i $(echo $i | awk -F / '{OFS=\"/\"} {print $1,$2,\"bl\"$3}'); done\n",
    "for i in cast/atac/ssBD*npf; do mv $i ${i/ssBD/blBD}; done\n",
    "\n",
    "#Check that all the files should have the same name prefix\n",
    "for i in */atac/*; do basename $i; done"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "With the files standardized, we can now extract the reads aligning to peak regions using distributed processing. Once the new files are generated, we use the SAMTools `calmd` command to produce BAM files with MD tags. This ensures that all the BAM files are following the same MD tag syntax. Our files are already sorted, but if they were not, it is *strongly* recommended to sort them first using `samtools sort`. This significantly improves the speed of `calmd`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "module load samtools/1.3.1\n",
    "for i in $(ls */atac/*bam | cut -f1,1 -d'.'); do bsub -q hour \"intersectBed -abam ${i}.sorted.bam -b ${i}.top50k.npf > ${i}.top50k.sorted.bam\"; done\n",
    "\n",
    "# Wait for previous jobs to finish before running\n",
    "for i in c57bl6/atac/*top50k.sorted.bam; do bsub -q hour \"samtools calmd -b ${i} /proj/fureylab/genomes/mouse/mm9_reference/mm9.fa > ${i/sorted.bam/sorted.md.bam}\"; done\n",
    "for i in cast/atac/*top50k.sorted.bam; do bsub -q hour \"samtools calmd -b ${i} /proj/fureylab/genomes/mouse/CAST/CASTEiJ_b37_f.fa > ${i/sorted.bam/sorted.md.bam}\"; done "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "The BAM files output from `calmd` should now be in the correct format for [read filtering](#Read-filtering)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "## RNA-seq data initial processing\n",
    "\n",
    "Our RNA-seq BAM files were aligned to reference transcriptomes, so we need to convert them back to genome coordinate space for further processing (SAMTools `calmd` requires files in genome coordinate space). RSEM has a convenient tool for converting transcript BAM files into genome BAM files using [`rsem-tbam2gbam`](http://deweylab.github.io/RSEM/README.html#aconvertingtranscriptbamfileintogenomebamfile). After cleaning up the filenames we apply `rsem-tbam2gbam`.\n",
    "\n",
    "**Note:** In converting transcriptome BAM files to genome BAM files with rsem-tbam2gbam the total number of reads in the genome file is reduced. This could be due to several factors, but I am not sure why this happens. Documentation from RSEM does not provide any answers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "module load rsem/1.2.31\n",
    "module load bedtools/2.25.0\n",
    "module load samtools/1.3.1\n",
    "\n",
    "cd /netscr/bquach/mutation_analysis\n",
    "\n",
    "#Clean up BAM filenames\n",
    "for i in c57bl6/rna/RNA*transcript.bam; do mv $i $(echo $i | awk -F - '{OFS=\"-\"} {print \"c57bl6/rna/\"$3}'); done\n",
    "for i in cast/rna/RNA*transcript.bam; do mv $i $(echo $i | awk -F - '{OFS=\"-\"} {print \"cast/rna/\"$3}'); done\n",
    "\n",
    "#Apply rsem-tbam2gbam\n",
    "for i in c57bl6/rna/*transcript.bam; do\n",
    "    bsub -q hour -n 4 -R \"span[hosts=1]\" \"rsem-tbam2gbam /proj/fureylab/genomes/mouse/mm9_rsem/rsem_refseq_jw ${i} ${i/transcript.bam/genome.bam} -p 4\";\n",
    "done\n",
    "for i in cast/rna/*transcript.bam; do\n",
    "    bsub -q hour -n 4 -R \"span[hosts=1]\" \"rsem-tbam2gbam /proj/fureylab/genomes/mouse/CAST_rsem/rsem_refseq_CAST ${i} ${i/transcript.bam/genome.bam} -p 4\";\n",
    "done"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "As a quality control pre-caution, we apply blacklist filtering. Following this we sort the results using `samtools sort` and input this into the SAMTools `calmd` command to produce BAM files with MD tags. This ensures that all the BAM files are following the same MD tag syntax. The sorting step significantly improves the speed of `calmd`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "#Blacklist filtering\n",
    "for i in c57bl6/rna/*genome.bam; do bsub -q hour \"intersectBed -v -abam ${i} -b /proj/fureylab/genomes/mouse/Blacklist/mm9_blacklist_raw_v2_3col_sorted_merged500bp.bed > ${i/genome.bam/genome.bl.bam}\"; done\n",
    "for i in cast/rna/*genome.bam; do bsub -q hour \"intersectBed -v -abam ${i} -b /proj/fureylab/genomes/mouse/Blacklist/CAST_blacklist_raw_3col_sorted_merged500bp.bed > ${i/genome.bam/genome.bl.bam}\"; done\n",
    "\n",
    "#Sort files\n",
    "for i in */rna/*genome.bl.bam; do bsub -q hour \"samtools sort ${i} -o ${i/bl.bam/bl.sorted.bam}\"; done\n",
    "\n",
    "#Apply calmd\n",
    "for i in c57bl6/rna/*genome.bl.sorted.bam; do bsub -q hour \"samtools calmd -b ${i} /proj/fureylab/genomes/mouse/mm9_reference/mm9.fa > ${i/genome.bl/genome.bl.md}\"; done\n",
    "for i in cast/rna/*genome.bl.sorted.bam; do bsub -q hour \"samtools calmd -b ${i} /proj/fureylab/genomes/mouse/CAST/CASTEiJ_b37_f.fa > ${i/genome.bl/genome.bl.md}\"; done "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "The BAM files output from `calmd` should now be in the correct format for [read filtering](#Read-filtering)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "## Read filtering\n",
    "\n",
    "To remove alignments that would be problematic for calculating mismatch statistics, we apply several alignment filtering criteria. These criteria are:\n",
    "\n",
    "* Alignments must be on a whitelist of references\n",
    "* Alignments meet default QC checks of the sequencing platform/vendor\n",
    "* Alignments must meet specified MAPQ score\n",
    "* Alignments must meet minimum PHRED quality score for specified fraction of bases\n",
    "* Alignments cannot contain indels\n",
    "* Alignments must be primary\n",
    "* Alignments cannot be a duplicate\n",
    "* Alignments must be mapped\n",
    "* Alignments cannot be hard or soft-clipped\n",
    "* Read and reference sequences of alignments cannot contain 'N' nucleotides \n",
    "\n",
    "All of these filtering steps have been implemented in the Python script `bam_filter.py`. This script accepts either BAM or SAM files, so if you have alignment files that contain `calmd` formatted `MD` and `NM` tags and meet the quality control standards outlined in the initial data processing steps, you can use those files directly with `bam_filter.py`. For our purposes, the whitelist acts as a way to exclude alignments to nonconventional references such as *chrM* or *chr1_random*. We use default values for all other arguments (`--phred 20 --percent 90 --mapq 30`). \n",
    "\n",
    "If `pysam` is not installed then you will need to install it first. If [pip](https://pip.pypa.io/en/stable/installing/) is installed then you can install `pysam` by using"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "pip install pysam --user"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "#Retrieve scripts from git repo\n",
    "cd /netscr/bquach/mutation_analysis\n",
    "git clone https://bryancquach@bitbucket.org/bryancquach/read-mismatch-classifier.git #only do if not yet cloned\n",
    "git pull #do this if cloned but not updated\n",
    "\n",
    "#Build whitelist\n",
    "echo -e \"chr1\\nchr2\\nchr3\\nchr4\\nchr5\\nchr6\\nchr7\\nchr8\\nchr9\\nchr10\\nchr11\\nchr12\\nchr13\\nchr14\\nchr15\\nchr16\\nchr17\\nchr18\\nchr19\\nchrX\\nchrY\" > whitelist.txt\n",
    "\n",
    "#Apply filtering\n",
    "for i in */rna/*genome.bl.md.sorted.bam; do \n",
    "    bsub -q hour \"python read-mismatch-classifier/mismatch_classifier/bam_filter.py ${i} ${i/genome.bl.md.sorted.bam/final.bam} whitelist.txt\"; \n",
    "done\n",
    "\n",
    "for i in */atac/*sorted.md.bam; do \n",
    "    bsub -q hour \"python read-mismatch-classifier/mismatch_classifier/bam_filter.py ${i} ${i/sorted.md.bam/final.bam} whitelist.txt\"; \n",
    "done"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "## Calculating initial total per base mismatch counts\n",
    "\n",
    "With the filtered BAM files, we can use `get_total_mm_profile.py` to generate total per base mismatches. This script will produce a tab-delimited text file containing the mismatches for every position in a read. As such, the read length of the sample must be specified. In our case the RNA-seq samples have read lengths of 48 and the ATAC-seq samples have read lengths of 50.\n",
    "\n",
    "The Python packages `numpy` and `scipy` will need to be installed if they haven't already."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "pip install numpy --user\n",
    "pip install scipy --user"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "#Show usage information\n",
    "python read-mismatch-classifier/mismatch_classifier/get_total_mm_profile.py --help\n",
    "\n",
    "#Calculate mismatches for RNA-seq samples\n",
    "for i in c57bl6/rna/*final.bam; do \n",
    "    bsub -q hour \"python read-mismatch-classifier/mismatch_classifier/get_total_mm_profile.py -o c57bl6/rna/ ${i} 48\"; \n",
    "done\n",
    "for i in cast/rna/*final.bam; do \n",
    "    bsub -q hour \"python read-mismatch-classifier/mismatch_classifier/get_total_mm_profile.py -o cast/rna/ ${i} 48\"; \n",
    "done\n",
    "\n",
    "#Calculate mismatches for ATAC-seq samples\n",
    "for i in c57bl6/atac/*final.bam; do \n",
    "    bsub -q hour \"python read-mismatch-classifier/mismatch_classifier/get_total_mm_profile.py -o c57bl6/atac/ ${i} 50\"; \n",
    "done\n",
    "for i in cast/atac/*final.bam; do \n",
    "    bsub -q hour \"python read-mismatch-classifier/mismatch_classifier/get_total_mm_profile.py -o cast/atac/ ${i} 50\"; \n",
    "done\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "## Aggregating total mismatches into an R matrix\n",
    "\n",
    "For exploratory analyses and other downstream analyses in R, code is provided for creating an R matrix that holds the total mismatches per base for each sample within a directory. The script to do this is `get_total_mm_matrix.R`. The matrix is stored as an RDS file that can be loaded in R using `readRDS()`. This functions similarly to `load()` for RData objects, excepts RDS objects can be assigned to a variable name of choice."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "bsub -q hour -M 1 \"Rscript read-mismatch-classifier/mismatch_classifier/get_total_mm_matrix.R c57bl6/rna/ c57bl6_rna_total_mm_matrix.rds\"\n",
    "bsub -q hour -M 1 \"Rscript read-mismatch-classifier/mismatch_classifier/get_total_mm_matrix.R cast/rna/ cast_rna_total_mm_matrix.rds\"\n",
    "bsub -q hour -M 1 \"Rscript read-mismatch-classifier/mismatch_classifier/get_total_mm_matrix.R c57bl6/atac/ c57bl6_atac_total_mm_matrix.rds\"\n",
    "bsub -q hour -M 1 \"Rscript read-mismatch-classifier/mismatch_classifier/get_total_mm_matrix.R cast/atac/ cast_atac_total_mm_matrix.rds\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "## Detecting outlier bases (preliminary)\n",
    "\n",
    "Due to sequencing error, some read positions have an unexpectedly high or low number of mismatches. To accomodate for this we apply a simple outlier detection method that assumes that within and across samples, most read positions have a consistent mismatch rate if you account for differences due to sequencing depth. The outlier detection approach we take first median-centers the mismatch count data for each sample then uses an aggregate distribution of normalized mismatch counts to determine outliers. An R code example of how this can be done is shown below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "c57 = readRDS(file=\"c57bl6_rna_total_mm_matrix.rds\")\n",
    "cast = readRDS(file=\"cast_rna_total_mm_matrix.rds\")\n",
    "rna = rbind(c57,cast)\n",
    "med.total = apply(t(rna), 2, median)\n",
    "med.total_centered = sweep(data.matrix(t(rna)), 2, med.total)\n",
    "#boxplot(med.total_centered)\n",
    "test = as.vector(med.total_centered)\n",
    "test.stats = boxplot(test)\n",
    "test.stats$stats #hinge values based on full distribution, mean-centered data\n",
    "#obtain median for each sample (normal scale, not centered) and find lower and upper value (-/+ hinge values from test.stats$stats):\n",
    "median = as.data.frame(med.total)\n",
    "median$lower = median$med.total - 1981.25\n",
    "median$upper = median$med.total + 6871.00\n",
    "write.table(median, file=\"MultiTissue_Mutation_Median_Hinge.txt\", sep=\"\\t\", quote=FALSE)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "## Calculating mismatch rates\n",
    "\n",
    "To generate a text file of mismatch rates for each sample, we can use the script `get_mm_rates.py`. The script provides the option to include or exclude a file with mismatch count cutoffs for determining outliers. The code below is shown for RNA-seq data only."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "for i in *c57bl6/rna/*final.bam; do \n",
    "    bsub -q hour \"python read-mismatch-classifier/mismatch_classifier/get_mm_rates.py -o c57bl6/rna/ -t MultiTissue_Mutation_Median_Hinge.txt ${i} 48\";  \n",
    "done\n",
    "for i in *cast/rna/*final.bam; do \n",
    "    bsub -q hour \"python read-mismatch-classifier/mismatch_classifier/get_mm_rates.py -o cast/rna/ -t MultiTissue_Mutation_Median_Hinge.txt ${i} 48\";  \n",
    "done"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "## Calculating grouped mismatch rates\n",
    "\n",
    "To generate a text file of grouped mismatch rates for each sample, we can use the script `get_grouped_mm_rates.py`. The script provides the option to include or exclude a file with mismatch count cutoffs for determining outliers. The code below is shown for RNA-seq data only."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "for i in *c57bl6/rna/*final.bam; do \n",
    "    bsub -q hour \"python read-mismatch-classifier/mismatch_classifier/get_grouped_mm_rates.py -o c57bl6/rna/ -t MultiTissue_Mutation_Median_Hinge.txt ${i} 48\";  \n",
    "done\n",
    "for i in *cast/rna/*final.bam; do \n",
    "    bsub -q hour \"python read-mismatch-classifier/mismatch_classifier/get_grouped_mm_rates.py -o cast/rna/ -t MultiTissue_Mutation_Median_Hinge.txt ${i} 48\";  \n",
    "done"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "## Aggregating mismatch rates into an R matrix\n",
    "\n",
    "For exploratory analyses and other downstream analyses in R, code is provided for creating an R matrix that holds the mismatch rate per mutation type for each sample within a directory. The script to do this is `get_mm_rates_matrix.R`. The matrix is stored as an RDS file that can be loaded in R using `readRDS()`. This functions similarly to `load()` for RData objects, excepts RDS objects can be assigned to a variable name of choice."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "# For individual mm types\n",
    "bsub -q hour -M 1 \"Rscript read-mismatch-classifier/mismatch_classifier/get_mm_rate_matrix.R c57bl6/rna/ c57bl6_rna_mm_rates_matrix.rds\"\n",
    "bsub -q hour -M 1 \"Rscript read-mismatch-classifier/mismatch_classifier/get_mm_rate_matrix.R cast/rna/ cast_rna_mm_rates_matrix.rds\"\n",
    "\n",
    "# For grouped mm types\n",
    "bsub -q hour -M 1 \"Rscript read-mismatch-classifier/mismatch_classifier/get_grouped_mm_rate_matrix.R c57bl6/rna/ c57bl6_rna_grouped_mm_rates_matrix.rds\"\n",
    "bsub -q hour -M 1 \"Rscript read-mismatch-classifier/mismatch_classifier/get_grouped_mm_rate_matrix.R cast/rna/ cast_rna_grouped_mm_rates_matrix.rds\"\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "## Plotting per base mismatch profiles (incomplete)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "source": [
    "## Comparing mismatch rates between sample groups (incomplete)\n",
    "\n",
    "To generate the list of samples for each strain and tissue, we used the code below. Because of inconsistent naming, we take into account the possibiliy of the tissue label being either capitalized or lowercase."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "for j in {Kidney,Liver,Lung}; do \n",
    "    for i in c57bl6/rna/*[\"${j}|$(echo ${j} | tr A-Z a-z)\"]*pkl; do \n",
    "        echo -e \"${i}\\t1\" >> c57bl6_rna_$(echo ${j} | tr A-Z a-z).txt; \n",
    "    done; \n",
    "done\n",
    "for j in {Kidney,Liver,Lung}; do \n",
    "    for i in cast/rna/*[\"${j}|$(echo ${j} | tr A-Z a-z)\"]*pkl; do \n",
    "        echo -e \"${i}\\t1\" >> cast_rna_$(echo ${j} | tr A-Z a-z).txt; \n",
    "    done; \n",
    "done\n",
    "for j in {Kidney,Liver,Lung}; do \n",
    "    for i in c57bl6/atac/*[\"${j}|$(echo ${j} | tr A-Z a-z)\"]*pkl; do \n",
    "        echo -e \"${i}\\t1\" >> c57bl6_atac_$(echo ${j} | tr A-Z a-z).txt; \n",
    "    done; \n",
    "done\n",
    "for j in {Kidney,Liver,Lung}; do \n",
    "    for i in cast/atac/*[\"${j}|$(echo ${j} | tr A-Z a-z)\"]*pkl; do \n",
    "        echo -e \"${i}\\t1\" >> cast_atac_$(echo ${j} | tr A-Z a-z).txt; \n",
    "    done; \n",
    "done"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "run_control": {
     "frozen": false,
     "read_only": false
    }
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  },
  "toc": {
   "colors": {
    "hover_highlight": "#DAA520",
    "running_highlight": "#FF0000",
    "selected_highlight": "#FFD700"
   },
   "moveMenuLeft": true,
   "nav_menu": {
    "height": "225px",
    "width": "252px"
   },
   "navigate_menu": true,
   "number_sections": true,
   "sideBar": true,
   "threshold": 4,
   "toc_cell": false,
   "toc_section_display": "block",
   "toc_window_display": false,
   "widenNotebook": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
